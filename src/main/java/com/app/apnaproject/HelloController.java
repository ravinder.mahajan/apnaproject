package  com.app.apnaproject;


import com.app.model.Project;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {

    @RequestMapping(value = "/projectname", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Project getProject(){

        return fetchResp();
    }
    public Project fetchResp() {
        return new Project("apna project");
    }
}

