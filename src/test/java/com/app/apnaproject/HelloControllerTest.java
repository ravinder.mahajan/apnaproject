package com.app.apnaproject;

import com.app.model.Project;
import org.junit.Assert;
import org.junit.Test;

public class HelloControllerTest {
    @Test
    public void shouldReturnHelloWorldApiResponse(){
        HelloController helloController= new HelloController();
        Project resp = helloController.fetchResp();
        String expected= "apna project";
        Assert.assertEquals(expected, resp.getName());

    }
}
