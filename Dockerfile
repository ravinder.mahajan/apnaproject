FROM openjdk:11.0.3-jdk-stretch

ADD /build/libs/apnaproject-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar", "/app.jar"]
